# imagen raiz
FROM node

# Carpeta Raiz
WORKDIR /apitechu

# copia de archivos
ADD . /apitechu

#añadir volumen
VOLUME ['/logs']

# Exponer puerto
EXPOSE 3000

# instalar dependencias
#RUN npm install

# comando de inicializacion
CMD ["npm", "start"]
