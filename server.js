//console.log("Hola MUNDO");
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); //podria ser bodyParser.text o .raw

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechummc/collections/";
var requestJson = require('request-json');
var mLabAPIKey = "apiKey=tMm3DVst4MhwBqYxuHk-HIUdBYNQDV8E";


var port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto BIP BIP BIP " + port);

app.get('/apitechu/v1',
function(reg, res) {
  console.log("GET /apitechu/v1");
  res.send({"msg" : "Bienvenido a la API de tech U"});
  //esta linea envia respuesta al postman
}
);
app.get('/apitechu/v1/users',
function(req, res){
console.log("GET /Apitechu/v1/users");
res.sendFile('usuarios.json',{root: __dirname}) // __dirname constante que esta en node
//res.senfile('./usuarios.json'); Deprectaed , pero pone un . para ver donde empieza, en cambio en la de arriba se pone root
} //Te devuelve el fichero en formato json en el postman
);
app.post('/apitechu/v1/users',
function (req,res){
console.log("POSTT /Apitechu/v1/users"); //Creamos un nuevo usuario
//console.log(req);
//res.send({"msg" : "Respuesta"});
var newUser = {
"first_name" : req.body.first_name,
"last_name" : req.body.last_name,
"country" : req.body.country
};
var users = require('./usuarios.json');
users.push(newUser);
writeUsersDataToFile(users);
res.send(users);
}
);
app.delete(
"/apitechu/v1/users/:id",
function(req,res){
  console.log("DELETE  /Apitechu/v1/users/:id");
  var users = require('./usuarios.json');
  users.splice(req.params.id-1,2); //que quite 1, y que mida 1 de longitud
  writeUsersDataToFile(users);
  res.send(users);
//   {
  //  "msg" : "Usuario borrado con exito"
//   }
//  );
}
)
function writeUsersDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile(
    "./a.json",
    jsonUserData,
    "utf8",
    function(err){
     if(err){
       console.log(err);
     }else {
       console.log("fichero de Usuarios persistido");
     }
    }
  )
}
app.post('/apitechu/v1/monstruo/:p1/:p2',
function(req,res) {

console.log("Parametros");
console.log(req.params);

console.log("Query String");
console.log(req.query);

console.log("Headers");
console.log(req.headers);

console.log("Body");
console.log(req.body);

}

)
app.post('/apitechu/v1/login',
function(req,res) {
 //console.log(req.body);
 //console.log(req.body.email);
 var users = require('./a.json');
// console.log(users);
 //res.sendFile('ficheropassword.json',{root: __dirname})
 //res.sendFile(users,{root: __dirname})
 var a = false;
 var b = false;
 for (user of users){
 if (user.email == req.body.email)
 {
   if (user.password == req.body.password)
   {
     console.log("paso 1");
     a = true;
     b = true;
     user.logged = true;
    writeUserpasswordDataToFile(users);
      console.log("paso AA");
     break;

   }
 }
 else {
   console.log("paso 2");
 }
 }
if (a == false) {
console.log("paso 3");
res.send({"mensaje" : "Login incorrectooooo"});
}




}
)



function writeUserpasswordDataToFile(data){
  //Incluimos la libreria fs y convertimos en string el json de usuarios
  //si no hacemos esto podría fallar porque el fichero esta en binario
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  //Escribimos el fichero de usuarios con password
  fs.writeFile("./a.json",
    jsonUserData,
    "utf8",
    function(err){
      if (err){
        console.log(err);
      }else{
        console.log("Usuario Persistido");
      }
    }
  );
  console.log("Usuario añadido con exito");
}





app.post('/apitechu/v1/logout',
  function (req,res){
    console.log("POST apitechu/v1/logout");

  //Recogemos los datos de Body. id
      var logoutUser ={
          "id" : req.body.id,
      }
  //Comprobamos los valores que llegan por body
      console.log("user: "+req.body.id);


    //Buscamos el id en la lista de usuarios para ver si esta logado
    //y si lo esta lo deslogamos
    var   users = require('./a.json');
    for (user of users){
      if (user.id==logoutUser.id && user.logged){
          delete user.logged;
          writeUserpasswordDataToFile(users);
          res.send({"msg": "Logout Correcto", "idUsuario" : user.id});
          break;
      }
    }
    res.send({"msg": "Logout incorrecto"});
  }
);










app.get('/apitechu/v2/users',
  function(req,res){
     console.log("GET /apitechu/v2/users");
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");
    console.log (baseMlabURL+"user?"+mLabAPIKey);
    httpClient.get("user?"+ mLabAPIKey,
       function(err, resMLab, body) {
         var response = !err ? body : {
           "msg" : "Error obteniendo usuarios."
         }
         res.send(response);
         }
       )
     }
);


//CUENTA ASOCIADA AL USUARIO
app.get('/apitechu/v2/users/:id/accounts',
  function(req,res){
     console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userid" : ' + id + '}';

    httpClient = requestJson.createClient(baseMlabURL);
    console.log (baseMlabURL+"account?"+ query + "&" + mLabAPIKey);

    httpClient.get("account?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuenta."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body;
           } else {
             response = {
               "msg" : "cuenta no encontrada.",
               "tama" : body.length,
               "id" : id
             };
             res.status(404);
           }
         }
         res.send(response);
       }
       )
     }
);










///CREAR CUENTA

app.post('/apitechu/v3/users/accounts',
  function(req,res){
     console.log("POST /apitechu/v3/users/accounts");
     console.log("el body: " + JSON.stringify(req.body));

    httpClient = requestJson.createClient(baseMlabURL);
    var putBody =  req.body;
    console.log(baseMlabURL + "account?"+ "&" + mLabAPIKey );

    httpClient.post("account?"+ "&" + mLabAPIKey, putBody,
    function(errPOST, resMLabPOST, bodyPOST) {
         if (errPOST) {
           response = {
             "msg" : "Error cuenta no creada."
           }
           res.status(500);
         } else {
            console.log("creamos cuentaaaaa");
             response = {
               "msg" : "creamos cuenta."

             };


         }
         res.send(response);
       }
       )
     }
);











//MOVIMIENTO ASOCCIADO A LA CUENTA
app.get('/apitechu/v2/users/:id/movi',
  function(req,res){
    console.log("GET /apitechu/v2/users/:id/movi");
    //filtramos por iban
    var id = req.params.id;
    //var id = "2";
    var query = 'q={"iban" : ' + JSON.stringify(id) + '}';
    httpClient = requestJson.createClient(baseMlabURL);
    console.log (baseMlabURL + "movi?"+ query + "&" + mLabAPIKey);
    httpClient.get("movi?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuenta."
           }
           console.log("Error al acceder a bd");
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body;
             console.log("Encuentra el registro");
         } else {
             response = "";
             console.log("No lo encuentraaaaa");
                }
         }
         res.send(response);
         console.log("Envio las respuesta al response");
       }
       )
     }
);












app.get('/apitechu/v2/users/:id',
  function(req,res){
     console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id" : ' + id + '}';

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");
    console.log (baseMlabURL+"user?"+ query + "&" + mLabAPIKey);

    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
       )
     }
);










app.post('/apitechu/v3/users/:id/movi',
  function(req,res){
     console.log("POST /apitechu/v3/users/:id/movi");

     var newmovi ={
         "id" : "",
         "iban" : req.body.iban,
         "concepto" : req.body.concepto,
         "fecha" : req.body.fecha,
         "importe" : req.body.importe,
         "balance" : ""

     }

    console.log("iban " + req.body.iban);
    console.log("concepto " + req.body.concepto);
    console.log("fecha " + req.body.fecha);
    console.log("fecha " + req.body.importe);
    var cuenta = JSON.stringify(req.body.iban);
    //cuenta="22";
    var query = 'q={"iban" : ' + cuenta + '}';
    var query2 = 'q={"iban" : ' + cuenta + '}';
    httpClient = requestJson.createClient(baseMlabURL);

    console.log (baseMlabURL+"account?"+ query + "&" + mLabAPIKey);

    httpClient.get("account?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuenta."
           }
           console.log("Error al buscar la cuenta");
           res.status(500);

         } else {
           if (body.length > 0) {
           //Encontramos al cuenta
             var saldo = body[0].balance;
             console.log("Saldo en cuentaaaaaaa" + saldo);
             newmovi.balance=saldo;
             console.log("Saldo en cuentaaaaaaa2 " + newmovi.balance);

             var query = '&s={id:-1}&l=1';
             console.log (baseMlabURL+"movi?" + query + "&" +mLabAPIKey);
             httpClient.get("movi?"+ query + "&" + mLabAPIKey,
             function(err, resMLab, body) {
                  if (err) {
                      response = {
                        "msg" : "Error al buscar el movimiento mas alto"
                      }
                      res.status(500);
                    } else {
                      if (body.length > 0) {
                        var idmovi = body[0].id;
                        console.log("el id mas alto es : " + idmovi );
                        newmovi.id=idmovi+1;
                        console.log("el id mas alto es : " + newmovi.id );
                      //Creamos el movimiento y luego actualizamos el saldo

                      var putBody = newmovi;
                      httpClient.post("movi?"+ "&" + mLabAPIKey,  putBody,
                      function(err, resMLab, body) {
                        if (err) {
                          response = {
                            "msg" : "Error insertando el movimiento."
                          }
                          res.status(500);
                        } else {
                            //Insertamos el movimiento y actaulizamos el saldo en la cuenta
                            console.log("Movimiento INSERTADOOOO");
                            httpClient.get("account?"+ query + "&" + mLabAPIKey,
                            function(err, resMLab, body) {
                                 if (err) {
                                   response = {
                                     "msg" : "Error obteniendo cuenta."
                                   }
                                   console.log("Error al buscar la cuenta");
                                   res.status(500);

                                 } else {
                                   if (body.length > 0) {
                                     console.log(" encontramos la cuenta");
                                    var saldoactualizado = (parseFloat(saldo) + parseFloat(newmovi.importe));
                                    console.log("SALDO" + saldoactualizado);
                                    console.log("IBAN" + body.iban);
                                    console.log("USERID" + body.userid);
                                    var putBody = {"$set" : {"balance" : saldoactualizado}};


                                     console.log("pasoooooo");
                                     console.log (baseMlabURL+"account?"+ query2 + "&" + mLabAPIKey, putBody);
                                     httpClient.put("account?" + query2 + "&" + mLabAPIKey, putBody,
                                     function (errPUT, resMLabPUT, bodyPUT){
                                       if (errPUT) {
                                         response = {
                                           "msg" : "Error actualizando el saldo cuenta."
                                         }
                                      }
                                      else {
                                        console.log("Actualizamos el saldo");
                                      }
                                    }
                                  )

                                }
                                }
                                }
                                )

                        }
                        //res.send(response);
                      }
                      )
                      } else {
                        response = {
                          "msg" : "movimiento no encontrado."
                        };
                        res.status(404);
                      }
                    }
                    //res.send(response);
                  }
                  )

           } else {
             response = {
               "msg" : "cuenta no encontrado."
             };
             res.status(404);
              console.log("NO ENCUENTRA la cuenta");

           }
         }
           res.send(response);
       }
       )
     }
);

/*
app.post('/apitechu/v2/login',
  function(req,res){
    console.log("POST /apitechu/v2/users/login");
    console.log(req.body.email);
    console.log(req.body.password);
    var query = 'q={"email": "' + req.body.email + '" , "password":"' + req.body.password +'"}';
    httpClient = requestJson.createClient(baseMlabURL);
    //console.log (baseMlabURL + "userrrrr?"+ query + "&&&&" + mLabAPIKey);
    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
          //   console.log("id : " + body[0].id);
          //   console.log("first_name : " + body[0].first_name);
          //   console.log("last_name : " + body[0].last_name);
          //   console.log("email : " + body[0].email);
          //   console.log("password : " + body[0].password);

           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         //res.send(response);
         //query del put

    var query = 'q={"id" : '+  body[0].id + '}';
    //para hacer el logout es unset. Hacer el logaout por parametro en vez de body
    var putBody = '{"$set" : {"logged" : true}}';
    //var putBody = '{"$set" : {"first_name" : "MAIR"}}';
    console.log (baseMlabURL + "user?"+ query + "&" + mLabAPIKey + " putbody " + putBody);
    httpClient.put("user?" + query + "&" + mLabAPIKey, putBody,
    function(errPUT, resMLabPUT, bodyPUT) {
           if (errPUT) {
             response = {"msg" : "Error modificando usuario."}
             res.status(500);
           } else {
               if (body.length > 0) {
                 response = body[0];
                 //response = {
                //   "msg" : "Usuario actualizadoooooooo.",
                 //};
                 console.log("EMAIL " + body[0].email);
                 console.log("PASSWORD " + body[0].password);
                 //console.log("LOGGED " + body[0].logged)
               } else {
                 response = {
                   "msg" : "Usuario no encontrado."
                 };
                 console.log("aaa" + bodyPUT.length);
                 res.status(404);

               }
              //res.send(response);
        }
         res.send(response);

       }
       )
       }
     );
     }
);
*/




/*
app.post('/apitechu/v2/login',
  function (req,res){
    console.log("POST apitechu/v2/login");

  //Recogemos los datos de Body. User,password,email
      var loginUser ={
          "password" : req.body.password,
          "email" : req.body.email
      }
  //Comprobamos los valores que llegan por body
      console.log("password: "+req.body.password);
      console.log("email: "+req.body.email);


    //Buscamos el mail en la lista de usuarios para ver si existe
    //Si existe le logamos y sino devolvemos error
    var query = 'q={"email" : "'+  loginUser.email +
                  '", "password" : "' + loginUser.password + '"}';

    //Generamos el cliente con la consulta http

    httpClient = requestJson.createClient(baseMlabURL);
    console.log ("Cliente Creado");
    console.log (baseMlabURL+"user?"+ query + "&" +mLabAPIKey);

    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         //Hacemos put para hacer logged = true
         //Solo si la query anterior ha obtenido resultado
        if (body.length > 0) {
           var query = 'q={"id" : '+  body[0].id + '}';
                      var putBody = '{"$set" : {"logged" : true}}';
           console.log (baseMlabURL+"user?"+ query + "&" + mLabAPIKey, JSON.parse(putBody));
                      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                                   function (errPUT, resMLabPUT, bodyPUT){
                                        if (errPUT) {
                                          response = {"msg" : "Error logando usuario."}
                                          res.status(500);
                                        } else {
                                          if (bodyPUT.length > 0) {
                                            response = body[0];
                                          } else {
                                            response = {"msg" : "Usuario a logar no encontrado."};
                                            res.status(404);
                                          }
                                        }
                     res.status(200);
                                        //res.send("Login Finalizado");
                                         }
                                     )
                  //Finalizamos el PUT
          }
          res.send("Login Finalizado");
       }
    )
  }
);

*/







app.post('/apitechu/v2/register',
  function (req,res){
    console.log("POST /apitechu/v2/register");

  //Recogemos los datos de Body. User,password,email
      var loginUser ={
          "id" : "",
          "password" : req.body.password,
          "email" : req.body.email,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name

      }



    //Buscamos el mail en la lista de usuarios para ver si existe
    //Si existe le logamos y sino devolvemos error
   var query = 'q={"email" : "'+  loginUser.email + '"}';


    httpClient = requestJson.createClient(baseMlabURL);

    console.log (baseMlabURL+"user?"+ query + "&" +mLabAPIKey);

    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
           res.send(response);
         } else {
           if (body.length > 0) {
             //response = body[0];
             response = {
               "msg" : "EmailExistente",
               "existe" : true
             };
             res.send(response);
                    //Finalizamos el PUT
           } else {
             console.log("NO encontramosssssssss el email");

                 //Aqui habria aque darlo de alta

             var queryy = '&s={id:-1}&l=1';
             console.log (baseMlabURL +"user?" + queryy + "&" + mLabAPIKey);
             httpClient.get("user?"+ queryy + "&" + mLabAPIKey,
             function(errget, resMLabget, bodyget) {
               if (errget){
                console.log("Error al buscar el mas alto");
                response = {
                  "msg" : "Error al buscar el usuario nuevo.",
                  "insertado" : false
                }
              } else{
                  console.log("llegamos al exe: " + bodyget.length);

                  if (bodyget.length > 0) {
                    console.log("paso1");
                     var idcustomer = bodyget[0].id;
                     console.log("idcustomer" + idcustomer);
                     loginUser.id = idcustomer + 1;
                     var putBody = loginUser;
                     console.log("putBody: " + putBody);
                     console.log("PARSEADO putBody: " + JSON.stringify(putBody));
                     httpClient.post("user?"+ "&" + mLabAPIKey,  putBody,
                     function(errPOST, resMLabPOST, bodyPOST) {
                       if (errPOST){
                        response = {
                          "msg" : "Error al escribir el usuario.",
                          "insertado" : false
                        }
                        res.status(500);
                        res.send(response);
                        }
                        else{
                              console.log("insertado");
                              response = {
                                "msg" : "creadoOK",
                                "insertado" : true
                              }
                              res.send(response);
                              console.log("response1: " + response.msg);
                              console.log("response1: " + response.insertado);
                        }

                      }
                    )
                  }
                  else{
                      console.log("llegamos al exe sin reg");
                      response = {
                      "msg" : "Error al buscar el usuario mas alto.",
                      "insertado" : false
                    }
                  }

              }


              }
            )



           }
           //console.log("response3: " + response.msg);

         }
         //console.log("response4: " + response.msg);


       }
    )
  }
);











app.post('/apitechu/v2/login',
  function (req,res){
    console.log("POST apitechu/v2/login");

  //Recogemos los datos de Body. User,password,email
      var loginUser ={
          "password" : req.body.password,
          "email" : req.body.email
      }
  //Comprobamos los valores que llegan por body
      console.log("password: "+req.body.password);
      console.log("email: "+req.body.email);


    //Buscamos el mail en la lista de usuarios para ver si existe
    //Si existe le logamos y sino devolvemos error
    var query = 'q={"email" : "'+  loginUser.email +
                  '", "password" : "' + loginUser.password + '"}';

    //Generamos el cliente con la consulta http

    httpClient = requestJson.createClient(baseMlabURL);
    console.log ("Cliente Creado");
    console.log (baseMlabURL+"user?"+ query + "&" +mLabAPIKey);

    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             //response = body[0];
             response = JSON.parse('{"id" : '+  body[0].id + ', "first_name" : "' +  body[0].first_name + '", "last_name" : "' +  body[0].last_name + '"}') ;
             //Hacemos put para hacer logged = true
             var query = 'q={"id" : '+  body[0].id + '}';
                        var putBody = '{"$set" : {"logged" : true}}';
             console.log (baseMlabURL+"user?"+ query + "&" + mLabAPIKey, JSON.parse(putBody));
                        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                                     function (errPUT, resMLabPUT, bodyPUT){
                                          if (errPUT) {
                                            response = {"msg" : "Error logando usuario."}
                                            res.status(500);
                                          } else {
                                            if (bodyPUT.length > 0) {
                                              //response = body[0];
                         response = JSON.parse('{"id" : '+  body[0].id + ', "first_name" : "' +  body[0].first_name + '", "last_name" : "' +  body[0].last_name + '"}') ;
                                            } else {
                                              response = {"msg" : "Usuario a logar no encontrado."};
                                              res.status(404);
                                            }
                                          }
                       res.status(200);
                                          //res.send("Login Finalizado");
                                           }
                                       )
                    //Finalizamos el PUT
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);






app.post('/apitechu/v2/logout',
  function (req,res){
    console.log("POST apitechu/v2/login");

  //Recogemos los datos de Body. User,password,email
      var loginUser ={
          "password" : req.body.password,
          "email" : req.body.email
      }
  //Comprobamos los valores que llegan por body
      console.log("password: "+req.body.password);
      console.log("email: "+req.body.email);


    //Buscamos el mail en la lista de usuarios para ver si existe
    //Si existe le logamos y sino devolvemos error
    var query = 'q={"email" : "'+  loginUser.email +
                  '", "password" : "' + loginUser.password + '"}';

    //Generamos el cliente con la consulta http

    httpClient = requestJson.createClient(baseMlabURL);
    console.log ("Cliente Creado");
    console.log (baseMlabURL+"user?"+ query + "&" +mLabAPIKey);

    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         //Hacemos put para hacer logged = true
         //Solo si la query anterior ha obtenido resultado
        if (body.length > 0) {
           var query = 'q={"id" : '+  body[0].id + '}';
                      var putBody = '{"$unset" : {"logged" : ""}}';
           console.log (baseMlabURL+"user?"+ query + "&" + mLabAPIKey, JSON.parse(putBody));
                      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                                   function (errPUT, resMLabPUT, bodyPUT){
                                        if (errPUT) {
                                          response = {"msg" : "Error logando usuario."}
                                          res.status(500);
                                        } else {
                                          if (bodyPUT.length > 0) {
                                            response = body[0];
                                          } else {
                                            response = {"msg" : "Usuario a logar no encontrado."};
                                            res.status(404);
                                          }
                                        }
                     res.status(200);
                                        //res.send("Login Finalizado");
                                         }
                                     )
                  //Finalizamos el PUT
          }
          res.send("Login Finalizado");
       }
    )
  }
);
